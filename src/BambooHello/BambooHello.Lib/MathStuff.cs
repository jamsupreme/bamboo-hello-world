﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambooHello.Lib
{
    public static class MathStuff
    {
        public static int Add(int x, int y)
        {
            if (x > 0)
            {
                throw new Exception("x should be negative");
            }
            if (y > 100)
            {
                throw new Exception("y cannot be larger than 100");
            }
            return x + y;
        }

        public static int Subtract(int x, int y)
        {
            return x + y;
        }
    }
}
