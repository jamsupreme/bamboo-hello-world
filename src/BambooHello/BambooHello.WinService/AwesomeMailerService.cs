﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BambooHello.WinService
{
    public partial class AwesomeMailerService : ServiceBase
    {
        readonly Timer _timer = new Timer { Interval = 1000 * 60 * 60 }; // send email every 5 mins

        public AwesomeMailerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //SendEmail(); //for now don't fire on start, I know it works
            _timer.Elapsed += (sender, eventArgs) =>
            {
                SendEmail();
            };
            _timer.Start();
        }

        protected override void OnStop()
        {
            _timer.Stop();
        }

        private void SendEmail()
        {
            const String FROM = "jspencer.jms@gmail.com";
            // Replace with your "From" address. This address must be verified.
            const String TO = "jspencer.jms@gmail.com"; // Replace with a "To" address. If you have not yet requested
            // production access, this address must be verified.

            const String SUBJECT = "Awesome octo mailer";
            const String BODY = "This email was sent through the Amazon SES SMTP interface by using C# via awesome service deployed via octo deploy.";

            // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
            const String SMTP_USERNAME = "AKIAIZUDXYYDLTXILW3A"; // Replace with your SMTP username. 
            const String SMTP_PASSWORD = "AtgaSGixpLdZOd3FW8ZMjhjCgOGJ/hcuo6vfNbWKd4pp"; // Replace with your SMTP password.

            // Amazon SES SMTP host name. This example uses the us-west-2 region.
            const String HOST = "email-smtp.us-east-1.amazonaws.com";

            // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use
            // STARTTLS to encrypt the connection.
            const int PORT = 587;

            // Create an SMTP client with the specified host name and port.
            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;

                // Send the email. 
                try
                {
                    EventLog.WriteEntry("Attempting to send an email through the Amazon SES SMTP interface...", EventLogEntryType.Information);
                    client.Send(FROM, TO, SUBJECT, BODY);
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Failed to send an email. Stack trace:" + ex.StackTrace, EventLogEntryType.Warning);
                }
            }
        }

    }
}
