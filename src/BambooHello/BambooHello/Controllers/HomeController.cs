﻿using System.Web.Mvc;
using BambooHello.Lib;

namespace BambooHello.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Number = MathStuff.Add(-1, 5);
            return View();
        }
    }
}