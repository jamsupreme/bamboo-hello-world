﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xbehave;
using Xunit.Should;

namespace BambooHello.Lib.Tests
{
    public class TestMathStuff
    {
        [Scenario]
        public void TestAdd()
        {
            int x = 1;
            int y = 1;
            ("Given x" + x + " and y " + y).f(() => { });
            "When adding".f(() => { });
            ("Then the output should be " + (x + y)).f(() =>
            {
                var expected = x + y;
                var actual = MathStuff.Add(x, y);
                actual.ShouldBe(expected);
            });
        }

        [Scenario]
        public void TestSubtract()
        {
            int x = 1;
            int y = 1;
            ("Given x" + x + " and y " + y).f(() => { });
            "When subtracting".f(() => { });
            ("Then the output should be " + (x - y)).f(() =>
            {
                var expected = x + y;
                var actual = MathStuff.Subtract(x, y);
                actual.ShouldBe(expected);
            });
        }
    }
}
